import numpy as np

import sys
sys.path.append('/scratch/kjersti/cao_model_v2/')
from modcor.modfun.fun_moisture import thermo_rh
from modcor.modfun.fun_heatflux import sfcflux_bulk
import initdata.init_fun as init
import modcor.constants as c

xmax      = 444            # How far you want to integrate (in km)
deltax    = 0.1             #Resolution (in km) (doesn't work well if you make it coarser)
x         = np.arange(deltax,xmax+deltax,deltax)*1e3

hepsilon = 0.002  # Convergence criterion 
N = 3000          # max  ABL height you expect through the iteration (how large you initialize arrays)


moisture  = True
ocean     = True
seaice    = True
tau       = 8*24*60*60 #"Length" of CAO outbreak on the ocean in seconds


# ABL parametrs
thetam0   = 253             # ABL mixed layer temperature
gammath0 = 0.012            # Background stability
um0     = 10                # mixed layer wind speed
h0      = 100               # Initial ABL height
qv0     = 0.30e-3           # Intial moisture content
qc0     = 0                 #No liquid water to start out with
upsilon = 0.2               #Entrainment parameter
qb0     = 0                 #background moistre profile; not IMPLEMENTED


# Ice quantities
ice_init  = np.zeros(len(x))#init.eaMIZ_fun(int(90/deltax), x, 'tanhL')#
OW    = 1-ice_init#np.ones(len(x))
SI      = 'none'


#Ocean mixed layer quantities
mld0    = 23           #Initial mixed layer depth
depth_m = 500

Tprofile0, Sprofile0 = init.CTD_cross_test(x)
for i in range(len(x)): 
    Tprofile0[i,0:mld0] = np.nanmean(Tprofile0[i,0:mld0])#Tm0[0]
    Sprofile0[i,0:mld0] = np.nanmean(Sprofile0[i,0:mld0])#Sm0[0]

Tprofile = Tprofile0.copy()
Sprofile = Sprofile0.copy()

Tm0 = Tprofile[:,0]
Sm0 = Sprofile[:,0]




# End of Part where you should change stuff! 



#-------------------------------------------
## Do some initial calculations
#-------------------------------------------
pm0       = 1000
a         = (pm0/1000)**c.K     #Coefficient for potential temperature
tm0       = thetam0*a         #Temperture at reference pressure given in K
T0        = tm0-273.15        #Initial temperature given in C
rhom0     = (pm0*100)/(c.R*tm0) #Mixed layer density using surface pressure
P0        = 0                 #Initial precipitation
#Initial values calculations
__,__,qsat0,__,__ = thermo_rh(thetam0,pm0,100)    #Saturation mixing ratio of the air

#saturation mixing ratio close to the surface. 98% to account for salinity (Talley)
__,__,qs0_sst,__,__= thermo_rh(Tm0[0],pm0,98)
qm0  = qv0 + qc0                              #Total mixing ratio
thL0 = thetam0 - c.Lv*thetam0/(c.cp*T0)*qc0   #liquid water potential Temperature
#Remember, that if you don't include moisture thL = thetam0

#Calculate the fluxes with the bulk flux routine
QH0, QE0  = sfcflux_bulk(Tm0[0],thL0,qs0_sst,qm0,um0)

#Multiply sensible heat flux with the fraction of open water:
hsf0 = QH0*OW[0]

#And same with latent heat flux:
lhf0 = QE0*OW[0]*moisture

#jump in temperature at the top of the mixed layer
dTH0 = gammath0*h0