import numpy as np

import sys
sys.path.append('/scratch/kjersti/cao_model_v2/')
from modcor.modfun.fun_moisture import thermo_rh
from modcor.modfun.fun_heatflux import sfcflux_bulk

import initdata.init_fun as init
import modcor.constants as c


#Domain setup: 
# -----------------------------------------------------------------------------
xmax      = 300
deltax    = 0.1             #Resolution (in km)
x         = np.arange(deltax,xmax+deltax,deltax)*1e3

hepsilon = 0.002 # Convergence criterion 
N        = 3000  # max  ABL height you expect through the iteration (how large you initialize arrays)

# Vary complexities: 
#_-----------------------------------------------------------------------------
moisture  = True
ocean     = True
seaice    = False
tau       = 2*24*60*60 #"Length" of CAO outbreak on the ocean in seconds
upsilon   = 0.2               #Entrainment parameter


# ABL parametrs
# -----------------------------------------------------------------------------
thetam0   = 253
gammath0  = 0.012
um0       = 10
h0      = 100
qv0     = 0.30e-3
qc0     = 0                 #No liquid water to start out with
qb0     = 0                 #background moisture profile; not sure if it actually works in this script. But if qb = 0; then there is no different from the original script. 

# Ice quantities
# -----------------------------------------------------------------------------
ice_init   = np.zeros(len(x))#init.eaMIZ_fun(int(90/deltax), x, 'step')#
OW    = 1-ice_init

mld0    = 50           #Initial mixed layer depth
N       = 3000
depth_m = 500
SI      = 'none'

# Ocean profile initialization
# -----------------------------------------------------------------------------
Tprofile = np.ones((len(x), depth_m))
Sprofile = Tprofile.copy()

Tprofile0, Sprofile0 = init.init_CTD(75.5, -7.5, mld0, ideal = True) 
Tprofile[:,:] = Tprofile0[0:depth_m]
Sprofile[:,:] = Sprofile0[0:depth_m]




for i in range(len(x)): 
    Tprofile[i,0:mld0] = np.nanmean(Tprofile[i,0:mld0])#Tm0[0]
    Sprofile[i,0:mld0] = np.nanmean(Sprofile[i,0:mld0])#Sm0[0]

Tprofile0 = Tprofile.copy()
Sprofile0 = Sprofile.copy()

Tm0 = Tprofile[:,0]
Sm0 = Sprofile[:,0]

Tm = Tm0.copy()
Sm = Sm0.copy()

# -----------------------------------------------------------------------------

# End of Part where you should change stuff! 

# -----------------------------------------------------------------------------

#-------------------------------------------
## Do some initial calculations
#-------------------------------------------
pm0       = 1000
a         = (pm0/1000)**c.K     #Coefficient for potential temperature
tm0       = thetam0*a         #Temperture at reference pressure given in K
T0        = tm0-273.15        #Initial temperature given in C
rhom0     = (pm0*100)/(c.R*tm0) #Mixed layer density using surface pressure
P0        = 0                 #Initial precipitation
#Initial values calculations
__,__,qsat0,__,__ = thermo_rh(thetam0,pm0,100)    #Saturation mixing ratio of the air

#saturation mixing ratio close to the surface. 98% to account for salinity (Talley)
__,__,qs0_sst,__,__= thermo_rh(Tm0[0],pm0,98)
qm0  = qv0 + qc0                              #Total mixing ratio
thL0 = thetam0 - c.Lv*thetam0/(c.cp*T0)*qc0   #liquid water potential Temperature
#Remember, that if you don't include moisture thL = thetam0

#Calculate the fluxes with the bulk flux routine
QH0, QE0  = sfcflux_bulk(Tm0[0],thL0,qs0_sst,qm0,um0)

#Multiply sensible heat flux with the fraction of open water:
hsf0 = QH0*OW[0]

#And same with latent heat flux:
lhf0 = QE0*OW[0]*moisture

#jump in temperature at the top of the mixed layer
dTH0 = gammath0*h0