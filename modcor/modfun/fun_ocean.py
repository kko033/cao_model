import numpy as np
import seawater as sw
import modcor.constants as c


def jumpO_fun(dTdz,mld,Hd,Td,dSdz,Sd,Tm,Sm):
    '''
    Creates an idealized profile based on some simple parameters 
    
    Parameters : 
    ------------
        mld : mixed layer depth
        dz  : 
        dTdz: gradient of thermocline 
        dSdz: gradient of halocline
        Td  : temperature in the deep ocean
        Sd  : salinity in the deep oicean
        Hd  : depth of the deep ocean
        Tm  : mixed layer temperature
        Sm  : mixed layer salinity 
    Returns:
    ----------
        DT : new mixed layer temperature 
        DS : new mixed layer salinity 


    '''
    
  
    Th = dTdz*(mld-Hd) + Td
    Sh = dSdz*(mld-Hd) + Sd

    DT = -(Tm - Th)
    DS = -(Sm - Sh)

    return DT,DS

#!! Todo: This function is horrible 
def ideal_convection_fun(mld,dz,dTdz,dSdz,Td,Sd,Hd,Tm,Sm):
    '''
    Checks for instabilities in an IDEALIZED ocean profile by comparing neighboring densities: 
    If the overlying density is larger than the
    underlying, the column is unstable. The function finds the index of this
    location, locate the density and find where the density of the pycnocline
    is similar to this density. It then mix down to this level. The temperature
    and salinity is also mixed to this level.

    Parameters : 
    ------------
        mld : mixed layer depth
        dz  : 
        dTdz: gradient of thermocline 
        dSdz: gradient of halocline
        Td  : temperature in the deep ocean
        Sd  : salinity in the deep oicean
        Hd  : depth of the deep ocean
        Tm  : mixed layer temperature
        Sm  : mixed layer salinity 
    Returns:
    ----------
        Tm : new mixed layer temperature 
        Sm : new mixed layer salinity 


    '''

    aa = 'something'

    while len(aa)>0:

        z         = np.arange(0,mld+dz,dz)
        zz        = np.arange(mld+dz,Hd,dz)
        T_profile = np.concatenate((np.ones(len(z))*Tm,dTdz*(zz-Hd) + Td),axis=0)
        S_profile = np.concatenate((np.ones(len(z))*Sm,dSdz*(zz-Hd) + Sd),axis=0)
     #   z_profile = np.concatenate((z,zz),axis=0)
        dens = sw.dens0(S_profile,T_profile-273.15)

        if T_profile[0]<sw.fp(S_profile[0],0):
            dd = sw.dens0(S_profile[0],T_profile[0])
            if dd > dens[0]:
                print('need to reconsider tihs density calculations!!!!' )
            

        stuff = np.zeros(len(dens))
        for i in range(1,len(dens)):
            stuff[i-1] = dens[i]-dens[i-1]
        aa = np.where(stuff<0)[0]
        if len(aa)>0:
            if mld<Hd:
                mld_new = mld + dz#z_profile[int((ccc-mld)/2)]   #Finds the depth this index corresponds to
                Tm_new = np.mean(T_profile[0:int(mld_new)])     #Mix down to this index
                Sm_new = np.mean(S_profile[0:int(mld_new)])     #Mix salinity down to this depth

                mld = mld_new
                Tm = Tm_new
                Sm = Sm_new


            else:
                print('No mixing can make the water light enough')
            #If the profiles are not unstable; return just the old values:
        Tm = T_profile[0]
        Sm = S_profile[0]
        mld= mld




    return Tm, Sm,mld


def profile_convection(mld,Tprofile,Sprofile):
    '''
    Checks for instabilities in an ocean profile by comparing neighboring densities: 
    If the overlying density is larger than the  underlying, the column is unstable.
     The function finds the index of this location, locate the density and find where 
    the density of the pycnocline is similar to this density. It then mix down to this level. 
    The temperature and salinity are also mixed to this level.

    Parameters : 
    ------------
        mld        : mixed layer depth
        Tprofile  : temperature profile
        Sprofile  : salinity profile
    Returns:
    ----------
        Tprofile : new mixed layer temperature profile
        Sprofile : new mixed layer salinity profile 
        mld : new mixed layer depth


    '''
    aa = True
    mld_new = 0
 
    while  aa:
        dens = sw.dens0(Sprofile,Tprofile-273.15)    
        dens_diff = np.diff(dens)
        if dens_diff[int(mld)-1]<0:
                aa = True
     
                mld_new = mld+1
                if mld_new < len(dens):
                    
                    Tm_new = np.mean(Tprofile[0:int(mld_new)])
                    Sm_new = np.mean(Sprofile[0:int(mld_new)])
                    
                else:
                    Tm_new = np.ones(len(Tprofile))*np.mean(Tprofile)
                    Sm_new = np.ones(len(Tprofile))*np.mean(Sprofile)
                    
                mld = mld_new
                 
                Tprofile = np.concatenate((np.ones(int(mld_new))*\
                                           Tm_new,Tprofile[int(mld_new):]),axis=0)
                Sprofile = np.concatenate((np.ones(int(mld_new))*Sm_new,\
                                           Sprofile[int(mld_new):]),axis=0)
        else:
            aa = False
    

    return  Tprofile,Sprofile,mld

def ice_fun(i,mld,Tprofile,Sprofile,ice):
    '''
    Quantifies changes that could occur due to ice growth
    but does this a posteriori. It returns hypothetical new functions of mld
    temperature- and salinity profiles in addition to the ice growth. 
    
    
    Parameters : 
    ------------
        mld        : (array) mixed layer depth
        Tprofile  : temperature profile
        Sprofile  : salinity profile
    Returns:
    ----------
        Tprofile : new mixed layer temperature profile
        Sprofile : new mixed layer salinity profile 
        mld : new mixed layer depth
        I   : sea ice thickness

    '''
    
    Sm = Sprofile[0]
    Tm = Tprofile[0]
    
    Tf = sw.fp(Sm,0)+273.15
    #I is the ice growth
    I = ((Tf-Tm)*c.cpO*c.rhoO*mld[i])/(c.rhoI*c.Li)
    if I <0:
        I = 0
    #DS is the salinity increase that would result from the ice growth   
    DS = c.rhoI * I * (Sm-c.S_ice)/c.rhoO/mld[i]
    Sm_new = Sm + DS

    if I>0:

        Tprofile[0:int(mld[i])] = np.ones(int(mld[i]))*Tf
        Sprofile[0:int(mld[i])] = np.ones(int(mld[i]))*Sm_new
        
        newT_prof, new_Sprof,new_mld = profile_convection(mld[i],Tprofile,Sprofile)
    
    else: 
        newT_prof = Tprofile
        new_Sprof = Sprofile
        new_mld = mld[i]
    
    
    return new_mld,newT_prof,new_Sprof, I
