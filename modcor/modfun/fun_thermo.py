import numpy as np
import sys
sys.path.append('..')
import modcor.constants as c


def thermo_es(t):
    '''
    Based on Renfrew and King 2000, modified by Kjersti.
    Calculates saturation vapour pressure wtr water and ice; returns ice
    first and then water from the polynomial approximation of Lowe (see Sargent 1980)
    
    Parameters : 
    -----------
        t : Temperature in centigrade
    Returns: 
    -----------
        esi : saturation vapour pressure wtr to ice
        esw : saturation vapour pressure wtr to water 

    '''
    #

    

    lowe_coef_ice = np.array([6.109177956, 5.03469897e-1, 1.886013408e-2,\
                     4.176223716e-4, 5.824720280e-6, 4.838803174e-8,\
                     1.838826904e-10])

    lowe_coef_water = np.array([6.107799961, 4.436518521e-1, 1.428945805e-2,\
                       2.650648471e-4,3.031240396e-6, 2.034080948e-8,\
                       6.136820929e-11])

    #Reverse the order of coefficient - does not have to do this in python!
    pesi = lowe_coef_ice[::-1]
    pesw = lowe_coef_water[::-1]

    esi = np.polyval(pesi,t)
    esw = np.polyval(pesw,t)

    return esi,esw



def thermo_rh(tk,p,rh):
    '''
    Generates thermodynamic variables from T[K] p(mb) rh(%) and gives out [theta, thetae, q, qsat, qsati]
    Based on Renfrew and King 2000, modified by Kjersti 2018.

    Parameters : 
    ------------
        tk    : temperature in kelvin
        p     : pressure in hPa
        rh    : relative humidity (%)
    Returns: 
    -----------
        theta : potential temperature
        thetae: equivalent potential temperature 
        r     : mixing ratio
        rsat  : saturation mixing ratio 
        rsati : saturation mixing ratio wtr to ice  

    '''
    #First; want all variables in SI-units
    p = p*100 #Converts from hPa to Pa

    #Calculate the potential temperature from temperature array
    p0 = 1000*100       #reference pressure in Pa
    R = c.R
    cp = c.cp

    K = R/cp            #kappa

    a = (p/p0)**(-K) #Constant for calculating the potential temperature
    theta = tk*a

    #calculate the equivalent potential temperature

    t = tk - 273.15
    #Function that calculates saturation vapour pressure wrt water and ice
    esi,es = thermo_es(t)

    es = es*100                #Convert to Pa
    esi = esi*100              #Convert to Pa
    rsat = c.eps*es/(p-es)       #Saturation mixing ratio wtr water

    rsati = c.eps*esi/(p-esi)    #saturation mixing ratio wtr ice

    e = es*rh / 100            #vapour pressure (Pa)

    #    qsat = rsat/(rsat+1)       #sat specific humidity wrt water
    #    qsati = rsati/(rsati+1) 	#sat specific humidity wrt ice


    #Calculate the water vapour mixing ratio, r; and specific humidity, q;
    r = c.eps*e/(p-e)
    q = r/(r+1)

    #Change the units to be g/kg
    Tlcl = 2840/(3.5*np.log(tk)-np.log(e/100)-4.805) + 55  # eqn 21, Bolton
    thetae = theta*np.exp(((3.376/Tlcl)-0.00254)\
                        *r*(1+0.81*r*1e-3)) # eqn 38, Bolton


    return thetae, q, r, rsat, rsati