import numpy as np

def sfcflux_bulk(sst, thetaL, qsw, qt, u): 
    '''
    Calculate heat fluxes based on bulk formulation

    Parameters:
    ------------
        sst    : sea surface temperatures
        thetaL : liquid water potential temperature
        qsw    :
        qt     : 
        u      : wind speed

    Returns
    ------------
        shf    : surface sensible heat flux
        lhf    : surface latent heat flux
    '''


    Ch = 1.5e-3  # Transfer coefficient for sensible heat
    Ce = 2.0e-3  # Transfer coeffieicnt for latent heat

    shf = Ch * u * (sst - thetaL)
    lhf = Ce * u * (qsw - qt)

    return shf, lhf
