
import numpy as np
import modcor.constants as c
from modcor.modfun.fun_thermo import thermo_rh


def cloud_fun(thL, qm, rhom): 
    '''
    Calculates the level of condensation assuming dry adiabatic conditions below the cloud level

    Parameteres:
        thL : liquid water potential temperature 
        qm  : Mixing ratio
        rhom: density of the ABL

    Returns: 
        zc : level of condensation (in m)
    '''


    g = c.g
    cp = c.cp

    zz = np.arange(0,3000)
    pres = (1e5 - rhom * g * zz)/100            #Pressure (from z to pressure)
    dTdz = - g / cp * zz + thL                  #Lapse rate
    __,__,__,dqsdz,__ = thermo_rh(dTdz,pres,100)   #Gradient of saturation specific humidity 
    cond = qm - dqsdz 

    #!! Todo: Rewrite this to be slightly nicer formulated
    cc = np.where(cond>0)
    cc = cc[0]

    if cc.size>0: 
        ccc = cc[0]
        zc  = zz[ccc]
    else: 
        zc = 9999

    return zc 



def precipRK4_fun(zc,thL,he,qm):
    '''
    Calculates the temperature and humidity in the air column using Runge Kutte 
    4th order method and assuming moist-adiabtic lapse rate

    Parameters: 
    -----------
        zc : level of condensation
        thL: liquid water potential temperature
        he : height of ABL
        rhom : density of ABL
        qm   : specific humidity of ABL

    Returns: 
    ----------
        P             :  Precipitation  
        qpprof        : precipitatble water profile
        qlprof        : liquid water profile
        theta_profile : profile of potential temperature 
        qvprof        : water vapour profile 
    '''



    h   = 1
    D   = he-zc
    z  = np.arange(0,int(zc),1)

    if int(D)>1:

        N = int(round(D/h))

        zz  = np.arange(0,N)
        z  = np.arange(0,int(zc),1)

        T_zc,p_zc = TPatZdry(zc,thL)
        __,__,__,qs,__ = thermo_rh(T_zc,p_zc,100)

        Q = np.zeros(N)
        T = np.zeros(N)

        Q[0] = qs
        T[0] = T_zc

        fQ  = lambda z,Q,T : - c.g/(c.R*T)*(0.622* c.Lv/(c.cp*T) - 1) * Q
        fT  = lambda z,Q,T : - c.g/c.cp * (1 + c.Lv**2/(c.cp*c.Rv*T**2) * Q)**(-1)

        for i in range(0,N-1):
            zz[i+1] = zz[i]+h

            k1Q     = fQ(zz[i]       ,Q[i]           ,T[i])
            k1T     = fT(zz[i]       ,Q[i]           ,T[i])

            k2Q     = fQ(zz[i] + h/2 ,Q[i] + h/2*k1Q ,T[i] + h/2*k1T)
            k2T     = fT(zz[i] + h/2 ,Q[i] + h/2*k1Q ,T[i] + h/2*k1T)

            k3Q     = fQ(zz[i] + h/2 ,Q[i] + h/2*k2Q ,T[i] + h/2*k2T)
            k3T     = fT(zz[i] + h/2 ,Q[i] + h/2*k2Q ,T[i] + h/2*k2T)

            k4Q     = fQ(zz[i] + h   ,Q[i] + h*k3Q   ,T[i] + h*k3T)
            k4T     = fT(zz[i] + h   ,Q[i] + h*k3Q   ,T[i] + h*k3T)

            Q[i+1]  = Q[i] + h/6*(k1Q + 2*k2Q + 2*k3Q + k4Q)
            T[i+1]  = T[i] + h/6*(k1T + 2*k2T + 2*k3T + k4T)


        ql     = qm - Q

        ql_threshold = 0

        qp = c.k1*(ql-ql_threshold)

        qp[qp<0]=0

        qlprof = np.concatenate((np.zeros(int(zc)),ql-qp))
        qpprof = np.concatenate((np.zeros(int(zc)),qp))
        qvprof = np.concatenate((np.ones(int(zc))*qm,Q))
        P = np.sum(qp) #Simple way of integrating it - when you have 1 m increments
        #in the vertical

        theta = thL + c.Lv/c.cp*qlprof

    else:
        P = 0
        qpprof = np.zeros(int(he))
        qlprof = np.zeros(int(he))
        theta = np.array([0])
        qvprof = np.ones(int(he))*qm


    theta_below = np.ones(len(z))*thL
    theta_profile = np.concatenate((theta_below,theta))
    return P,qpprof,qlprof,theta_profile,qvprof


def Tprofilev3_fun(qlprofile,thL):
    '''
    Calculates potential temperature profile from thetaL. 
    Potential temperature is not conserved because we have diabatic processes. 

    Parameters: 
    -----------
        qlprofile : liquid water profile 
        thL       : liquid water potential temperature 
    Returns : 
        thetaProf : potential temperature profile
    '''


    thetaProf = thL + c.Lv/c.cp * qlprofile

    return thetaProf

def TPatZdry(zc,thL):
    '''
    NB! Only for unsaturated conditions! 

    Calculates temperature and pressure at cloud base by using the potential temperature 
    equation solved for p and by assuming the temperature gradient below the cloud layer 
    to follow the dry adaiabtic lapse rate 

    Parameters: 
    -----------
        zc : level of condensation 
        thL : liquid water potential temperature 
    Returns: 
    ---------
        T_zc : Temperatuer at Zc
        P_zc : Pressure at zc
    '''

    gammaa = c.g/c.cp
    T_zc  = -gammaa*zc + thL
    p_zc  = c.p0/(thL/T_zc)**(c.cp/c.R)

    return T_zc, p_zc
