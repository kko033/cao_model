#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 15:16:55 2022

@author: kko033
"""


#%%
import sys
sys.path.append('/scratch/kjersti/cao_model_v2/')
from main import modCor

#%%
# INstead of using these classes, you can just import the initial values directly
#and pass them onto the modCor function... not sure about the best practice here.   
class reference(object):
    name = 'reference'
    def __init__(self, name):
        self.name = name
    import initdata.modinit_reference as mi
    
class test(object): 
    name = 'ocean_prof_test_NABOS_SI_T8_444'
    def __init__(self, name):
        self.name = name
    import initdata.modinit_ocean_prof_test as mi
    
#%%

for t in [test]:

    print(t.name)
  
    ds = modCor(t.mi)
    comp = dict(zlib=True, complevel=9)
    encoding = {var: comp for var in ds.data_vars}    

    ds.to_netcdf('/scratch/kjersti/cao_model_v2/runs/{}.nc'.format(t.name), encoding=encoding)

