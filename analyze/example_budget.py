import pandas as pd
import xarray as xr
import calc_budgets as cb
import proplot as pplt


ds = xr.open_dataset('/scratch/kjersti/cao_model_v2/runs/reference.nc')
budgets =  ['qv','ql','heat']
df_b = {}
for budget in budgets: 
    df = cb.totalBudgets(ds, [budget])
    df_b[budget] = df

#%%



fig,axs = pplt.subplots(ncols = 3, sharey=False, width = 8)
for budget, ax in zip(budgets, axs): 
    ax.bar(df_b[budget])
    ax.format(title=budget)
axs.format(xrotation=45)

