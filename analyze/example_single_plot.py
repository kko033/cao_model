#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 14 10:06:15 2022

@author: kko033
"""

import sys
sys.path.append('/scratch/kjersti/cao_model_v2/')
import analyze.base_plot as bp
import xarray as xr
import analyze.calc_budgets as cb
import proplot as pplt


case = 'ocean_prof_test_NABOS_SI_T8_444'

ds = xr.open_dataset('/scratch/kjersti/cao_model_v2/runs/{}.nc'.format(case))

#%% Baseplots
fig,ax = bp.base_plot(ds)

fig.save('/scratch/kjersti/cao_model_v2/analyze/gallery/{}_base.jpg'.format(case), dpi = 200)


#%% ABL plot
fig,ax = bp.ABLprofiles_plot(ds)
fig.save('/scratch/kjersti/cao_model_v2/analyze/gallery/{}_ABL.jpg'.format(case), dpi = 200)

#%% OML plot
fig,ax = bp.OMLprofiles_plot(ds)
fig.save('/scratch/kjersti/cao_model_v2/analyze/gallery/{}_OML.jpg'.format(case), dpi = 200)
#%% Calculate the budgets: 
budget_name =['qv','ql','heat']
budgets = cb.totalBudgets(ds,budget=budget_name)


fig,axs = pplt.subplots(ncols = 3, sharey=False)
for ax,b in zip(axs,budgets): 
    ax.bar(budgets[b].drop(['calc']))
axs.format(xrotation=45, collabels=[*budgets])
fig.save('/scratch/kjersti/cao_model_v2/analyze/gallery/{}_budgets.jpg'.format(case), dpi = 200)
