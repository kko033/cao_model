import proplot as pplt
import sys
sys.path.append('/scratch/kjersti/cao_model_v2/')
import analyze.base_plot as bp
import xarray as xr
import glob
import numpy as np
import analyze.calc_budgets as cb

#%%
to_comp = '/scratch/kjersti/cao_model_v2/runs/complexities/*moistTrue*ups2*'

files = glob.glob(to_comp)
files.sort()
comp_dict = {}

for file in files: 
    print(file)
    comp_dict[file.split('/')[-1][0:-3].split('_')[1]] = xr.open_dataset(file)

kkeys = [k for k in comp_dict]



print(kkeys)

#%%
fig,axs = bp.base_plot(comp_dict)



#%%

fig2, axs2 = bp.ABLprofiles_plot(comp_dict[kkeys[0]])

#%%

fig3, axs3 = bp.OMLprofiles_plot(comp_dict[kkeys[-1]])


#%% Complexities plots: 
import pandas as pd
to_comp = '/scratch/kjersti/cao_model_v2/runs/SI/*'

files = glob.glob(to_comp)
files.sort()



taus = list(set([f.split('/')[-1].split('_')[1] for f in glob.glob(to_comp+'*')]))
taus.sort()

ups = list(set([f.split('/')[-1][0:-3].split('_')[-1] for f in glob.glob(to_comp)]))
ups.sort()

moists = list(set([f.split('/')[-1][0:-3].split('_')[2] for f in glob.glob(to_comp)]))
hsf = {}
lhf = {}
for moisture in moists: 
    lhf[moisture] = pd.DataFrame(index = taus, columns = ups, dtype =float)
    hsf[moisture] = pd.DataFrame(index = taus, columns = ups, dtype =float)
    
    for tau in taus: 
        for up in ups: 
            
            file = glob.glob('{}*{}*{}*{}*'.format(to_comp,tau,moisture,up))[0]
            print(file)
            
            ds = xr.open_dataset(file)
            _hsf, _lhf = cb.int_heatflux(ds)
            hsf[moisture].loc[tau,up] = _hsf
            lhf[moisture].loc[tau,up] = _lhf      


#%%
fig,axs = pplt.subplots(ncols = 2, nrows=2, width = 10)
for m,ax in zip(moists,axs[0,:]): 
    ax.heatmap(hsf[m]/1e6, labels = True, precision=0, extend = 'both', cmap = 'viridis', vmin = 6e3, vmax = 9e3)
for m,ax in zip(moists,axs[1,:]): 
    ax.heatmap(lhf[m]/1e9, shading = 'flat', labels = True, precision=0, cmap = 'viridis' , vmin = 1e4, vmax = 1.4e4)    
axs.format(collabels=moists, ylabel = r'$\tau$ [days]', rowlabels = ['sensible heat flux', 
                                                                     'latent heat flux'], 
           xrotation = 45)

