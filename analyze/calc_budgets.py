import sys
sys.path.append('/scratch/kjersti/cao_model_v2')
import xarray as xr
import modcor.constants as c
import numpy as np
from scipy.ndimage import gaussian_filter1d as smooth
import pandas as pd


def top_fun(ds,VAR):
    #Calculate the value at the top of the ABL in a profile
    #Initialize
    thing_top = np.zeros(len(ds.he))
    data  = ds[VAR].values
    he    = ds.he.values
    for i in range(0,len(ds.he)):
        thing_top[i] = data[int(he[i])-1,i]
        
        
    return thing_top
def fun_gradvar(ds, var): 
    '''
    Function that calculates the mean in the layer and then the gradient in the x-direction
    
    (replace gradmoisture_fun)

    Parameters:
    -----------
        ds: xr dataset 
        var: (string)
            Variable you want to calculate the mean and gradient of

    Returns: 
    ----------
        dvardx : numpy array    
            gradient of the mean of the var
        varm   : numpy array
            the mean of var in the layer 
    '''

    #Smooth it because it is very unsmooth
    varm = ds[var].where(ds.z<ds.he).sum(dim='z')/ds.he
    var_smooth = smooth(varm, sigma = 15)
    dvardx = np.diff(var_smooth)/ds.dx

    return np.insert(dvardx,0,0)
    

def __qvBudget(ds, we): 

    cond         = - np.nan_to_num(fun_gradvar(ds,'ql_profile'))*ds.he*ds.um0    # Vapour is "lost" to liquid
    sfcflux      = ds.lhf / (ds.rho * c.Lv) #Vapour is gained through latent fluxes
    entrainment  = - we * top_fun(ds, 'qv_profile') # Vapour is lost to the free atmosphere
    P            = - smooth(ds.P/(24*60*60), sigma = 15) # Vapour is also lost to precipitation 
    condensation = cond + P - we * top_fun(ds, 'ql_profile') # Total loss is therefore condensation + precipitation + entrainment 
    loss         = np.ones(len(ds.x))*np.nan

    return condensation, sfcflux, entrainment, loss

def __heatBudget(ds, we): 
    cond,__,__,__ = __qvBudget(ds, we)
    loss          = - smooth(ds.P/(24*60*60), sigma = 15)
    condensation  = np.abs(cond) * c.Lv / c.cp
    entrainment   = we * ds.dTH
    sfcflux       = ds.hsf / (ds.rho * c.cp)
    
    return condensation, sfcflux, entrainment, loss
    
def __qlBudget(ds, we): 
    cond,__,__,__ = __qvBudget(ds, we)
    cond          = np.abs(cond)
    loss          = - smooth(ds.P/(24*60*60), sigma = 15)
    entrainment   = - we * top_fun(ds, 'ql_profile')
    sfcflux       = np.ones(len(ds.x))*np.nan
    
    return cond, sfcflux, entrainment, loss
    
def __qmBudget(ds, we): 
    condensation    = np.ones(len(we))*np.nan
    entrainment     = - we * ds.qm
    loss            = - ds.P/(24*60*60)
    sfcflux         = ds.lhf/ (c.Lv * ds.rho)

    return condensation, sfcflux, entrainment, loss



def totalBudgets(ds, budget = ['qv']): 
    xstart = 0
    xend   = -1

    def __prof_mean(VAR): 
        _vstart = ds.isel(x=xstart)
        _vend   = ds.isel(x=xend)
        m = (_vend[VAR].where(_vend.z<_vend.he).mean(dim='z')-_vstart[VAR].where(_vstart.z<_vstart.he).mean(dim='z')).values
            
        return m


    budget_dict = {'qv' : __qvBudget,
                    'ql' : __qlBudget,
                    'heat':__heatBudget,
                    'qm':__qmBudget}

    budget_var  = {'qv' : __prof_mean('qv_profile'),
                    'ql' : __prof_mean('ql_profile'),
                    'heat':__prof_mean('theta_profile'),
                    'qm': __prof_mean('qv_profile')}

    df_budgets = {}
    
    # Calculating the entrainment velocity 
    if ds.ups>0: 
        we = ds.ups * ds.hsf/(ds.rho* c.cp *  ds.dTH)
    else: 
        we =np.zeros(len(ds.x))

    for b in budget: 
        f = budget_dict[b]
        cond, flux, entr, loss = f(ds,we)
   
        cond1            = np.nan_to_num(cond / (ds.he * ds.um0))
        flux1            = np.nan_to_num(flux / (ds.he * ds.um0))
        entr1            = np.nan_to_num(entr / (ds.he * ds.um0))
        loss1            = np.nan_to_num(loss / (ds.he * ds.um0))
        
        condensation     = int_tot(cond1,xstart,xend,ds.dx) 
        sfcflux          = int_tot(flux1,xstart,xend,ds.dx) 
        entrain          = int_tot(entr1,xstart,xend,ds.dx) 
        totloss          = int_tot(loss1,xstart,xend,ds.dx) 
        
        total           = np.nansum((condensation,sfcflux,totloss,entrain))
    
        df = pd.Series(index = ['condensation','sfc flux','entrainment','loss','total','calc'],
                    data=[condensation, sfcflux, entrain, totloss,total,budget_var[b]], dtype=float)
        df_budgets[b] = df
    return df_budgets


def int_tot(var,xstart,xend,dx): 
    '''
    Calculates the integrated quantity using the trapezoid integration method

    Parameters
    ----------
    ds : datset
        dataset, model output.
    var : string
        variable name.
    xstart : float/int
        start of integration.
    xend : float/int
        end of integration.

    Returns
    -------
    tot : float
        Integrated quantity var.

    '''    
    tot = 0.5 * dx * (var[xstart] + var[xend] + 
                         2*np.nansum(var[xstart:xend]))
    
    return tot


def int_heatflux(ds): 
    '''
    Calculates the integrated sensible and latent heat flux into 
    the boundary layer 

    Parameters
    ----------
    ds : xr dataset
        MODEL OUTPUT.

    Returns
    -------
    sens_in : float
        Integrated sensible heat input.
    lat_in : float
        Integrated latent heat input.

    '''
    sens_in = np.sum(ds.hsf * c.cp * ds.rho)*ds.dx/ds.um0
    lat_in  = np.sum(ds.lhf * c.Lv * ds.rho)*ds.dx/ds.um0
    
    return sens_in, lat_in